from flask import Flask
from flask_mail import Mail, Message
from nums_api.subscription.models import EmailScriptLog, Subscription
from nums_api.trivia.models import Trivia
from nums_api.years.models import Year
from nums_api.maths.models import Math
from nums_api.dates.models import Date
from flask import render_template
from nums_api.database import db

app = Flask(__name__)

# These parameters come from mailtrap. Create a free acount and use their email testing services.
# Look for flask in the integrations dropdown bar
app.config['MAIL_SERVER'] = 'smtp.mailtrap.io'
app.config['MAIL_PORT'] = 2525
app.config['MAIL_USERNAME'] = 'f529091340af9f'
app.config['MAIL_PASSWORD'] = 'f8806f57cb2e67'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False
mail = Mail(app)


def send_new_fact_email():
    """Controller function"""
    update_fact_and_subscriber_counts()
    if new_facts_added():
        send_notification_email()


def update_fact_and_subscriber_counts():
    """Gets current total facts and subscribers and updates email database"""
    current_vol_of_facts = get_current_fact_volume()
    this_week_vol_of_subscribers = get_subscriber_count()

    new_email = EmailScriptLog(num_facts=current_vol_of_facts,
                               num_subscribers=this_week_vol_of_subscribers)

    db.session.add(new_email)
    db.session.commit()


def new_facts_added():
    """Checks if new fact(s) were added"""
    current_vol_of_facts = get_current_fact_volume()
    last_week_vol_of_facts = get_last_week_fact_volume()
    return current_vol_of_facts > last_week_vol_of_facts


def get_current_fact_volume():
    """Gets the current volume of facts"""
    return (db.session.query(Math.id).distinct().count() +
            db.session.query(Trivia.id).distinct().count() +
            db.session.query(Year.id).distinct().count() +
            db.session.query(Date.id).distinct().count())


def get_last_week_fact_volume():
    """Checks the emails database to get the latest fact_volume"""
    return db.session.query(EmailScriptLog.num_facts ).order_by(
        EmailScriptLog.checked_at.desc()).limit(1).scalar()


def get_subscriber_count():
    """Checks the total amount of subscribed emails"""
    return db.session.query(Subscription.id).distinct().count()


def get_new_facts():
    """Gets all new facts from each category"""
    last_week_timestamp = db.session.query(
        db.func.max(EmailScriptLog.checked_at)).scalar()

    new_fact_trivia = db.session.query(Trivia.fact_statement).filter(
        Trivia.added_at > last_week_timestamp).all()

    new_fact_math = db.session.query(Math.fact_statement).filter(
        Math.added_at > last_week_timestamp).all()

    new_fact_date = db.session.query(Date.fact_statement).filter(
        Date.added_at > last_week_timestamp).all()

    new_fact_year = db.session.query(Year.fact_statement).filter(
        Year.added_at > last_week_timestamp).all()

    new_facts = [[fact[0], category] for fact, category in zip(
        [new_fact_trivia, new_fact_math, new_fact_date, new_fact_year],
        ["Trivia", "Math", "Date", "Year"]) if fact]

    return new_facts


def get_subscribed_users_emails():
    """Gets all the subscribed emails and returns it as a list"""
    subscriptions = db.session.query(Subscription.email).all()
    list_of_subscribed_users_email = [s.email for s in subscriptions]
    return list_of_subscribed_users_email


def send_notification_email():
    """Sends notification email"""
    current_vol_of_facts = get_current_fact_volume()
    last_week_vol_of_facts = get_last_week_fact_volume()
    num_of_new_facts = current_vol_of_facts - last_week_vol_of_facts

    new_facts = get_new_facts()
    subscribed_users_emails = get_subscribed_users_emails()
    msg = Message(
        "New Fact(s) Alert!!!!",
        sender="numsAPI@mail.com",
        recipients=subscribed_users_emails
    )

    msg.html = render_template('email_template.html',
                               current_vol_of_facts=current_vol_of_facts,
                               last_week_vol_of_facts=last_week_vol_of_facts,
                               new_facts=new_facts,
                               num_of_new_facts=num_of_new_facts
                               )

    try:
        mail.send(msg)
        db.session.query(db.func.max(EmailScriptLog.checked_at)).limit(
            1).update({EmailScriptLog.email_sent: True})
        db.session.commit()
    except Exception as e:
        print(f"An error occurred while sending the email: {e}")
