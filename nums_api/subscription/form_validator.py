from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import InputRequired, Email


class SubscriptionForm(FlaskForm):
    "Email Validator"
    class Meta:
        csrf = False

    newemail = StringField(
        "Email",
        validators=[InputRequired(), Email()])
