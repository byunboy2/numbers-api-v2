from unittest import TestCase
from nums_api import app
from nums_api.database import db, connect_db
from nums_api.config import DATABASE_URL_TEST
from nums_api.subscription.models import Subscription
from email_validator import validate_email, EmailNotValidError


app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URL_TEST
app.config["TESTING"] = True
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

connect_db(app)

db.drop_all()
db.create_all()


class SubscriptionModelTestCase(TestCase):
    def setUp(self):
        """Set up test data here"""

        Subscription.query.delete()

        self.s1 = Subscription(
            email="123fake@fake.com"
        )

    def tearDown(self):
        """Clean up any fouled transaction."""
        db.session.rollback()

    def test_setup(self):
        """Test to make sure tests are set up correctly"""
        test_setup_correct = True
        self.assertEqual(test_setup_correct, True)

    def test_model_valid_email(self):
        """Test valid email"""
        self.assertIsInstance(self.s1, Subscription)
        self.assertEqual(Subscription.query.count(), 0)

        db.session.add(self.s1)
        db.session.commit()

        self.assertEqual(len(Subscription.query.all()), 1)

        email_obj = Subscription.query.filter_by(
            email="123fake@fake.com").one()
        self.assertEqual(email_obj.email, "123fake@fake.com")

    def test_model_invalid_email(self):
        """Test invalid email"""
        db.session.add(self.s1)
        db.session.commit()

        with self.assertRaises(ValueError):
            self.s2 = Subscription(
                email="123fakefake"
            )
            db.session.add(self.s2)
            db.session.commit()

            current_subscribers = Subscription.query.count()
            self.assertEqual(current_subscribers, 1)
