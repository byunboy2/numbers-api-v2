from unittest import TestCase
from nums_api import app
from nums_api.database import db, connect_db
from nums_api.config import DATABASE_URL_TEST
from nums_api.subscription.models import Subscription
from nums_api.__init__ import limiter


app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URL_TEST
app.config["TESTING"] = True
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

connect_db(app)

db.drop_all()
db.create_all()


class SubscriptionRouteTestCase(TestCase):
    """
        Houses setup functionality.
        Should be subclassed for any subscription route classes utilized
    """

    def setUp(self):
        """Set up test data here"""

        Subscription.query.delete()
        self.s1 = Subscription(
            email="123fake@fake.com"
        )

        db.session.add(self.s1)
        db.session.commit()

        self.client = app.test_client()

        # # disable API rate limits for tests
        limiter.enabled = False

    def tearDown(self):
        """Clean up any fouled transaction."""
        db.session.rollback()


class SubscriptionBaseRouteTestCase(SubscriptionRouteTestCase):
    """
    Test to see what would happen if a subscription email is added
    successfully to the database.
    """

    def test_valid_email(self):
        with self.client as c:
            resp = c.post("api/subscriptions/add_subscriber",
                          data={"newemail": "valid@email.com"})
            expected_resp = {"message": "Thank you for subscribing!"}
            self.assertEqual(resp.status_code, 201)
            self.assertEqual(resp.json, expected_resp)

    def test_invalid_email(self):
        with self.client as c:
            resp = c.post("api/subscriptions/add_subscriber",
                          data={"newemail": "email.com"})
            expected_resp = {"error": "Invalid email address!"}

            self.assertEqual(resp.status_code, 400)
            self.assertEqual(resp.json, expected_resp)

    def test_duplicate_email(self):
        with self.client as c:
            resp = c.post("api/subscriptions/add_subscriber",
                          data={"newemail": "123fake@fake.com"})
            expected_resp = {
                "error": "This email is already on the subscription list"}

            self.assertEqual(resp.status_code, 400)
            self.assertEqual(resp.json, expected_resp)
