from unittest import TestCase
from nums_api import app
from nums_api.database import db, connect_db
from nums_api.config import DATABASE_URL_TEST
from nums_api.subscription.models import EmailScriptLog


app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URL_TEST
app.config["TESTING"] = True
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

connect_db(app)

db.drop_all()
db.create_all()


class EmailModelTestCase(TestCase):
    def setUp(self):
        """Set up test data here"""

        EmailScriptLog.query.delete()

        self.e1 = EmailScriptLog(
            num_facts=5,
            num_subscribers=5
        )

    def tearDown(self):
        """Clean up any fouled transaction."""
        db.session.rollback()

    def test_setup(self):
        """Test to make sure tests are set up correctly."""
        test_setup_correct = True
        self.assertEqual(test_setup_correct, True)

    def test_model_valid_email(self):
        """Test the script saves email and fact volume, and current time when it's ran."""
        self.assertIsInstance(self.e1, EmailScriptLog)
        self.assertEqual(EmailScriptLog.query.count(), 0)

        db.session.add(self.e1)
        db.session.commit()

        self.assertEqual(EmailScriptLog.query.count(), 1)

        last_week_timestamp = db.session.query(
            db.func.max(EmailScriptLog.checked_at)).scalar()

        email_obj = EmailScriptLog.query.filter_by(
            checked_at=last_week_timestamp).scalar()

        self.assertEqual(email_obj.num_facts, 5)
        self.assertEqual(email_obj.num_subscribers, 5)
