from unittest import TestCase
from nums_api import app
from flask_mail import Mail
from unittest import mock
from nums_api.subscription.models import EmailScriptLog, Subscription
from nums_api.subscription.flask_mail_script import send_new_fact_email, get_current_fact_volume, get_last_week_fact_volume, get_new_facts
from nums_api.trivia.models import Trivia
from nums_api.years.models import Year
from nums_api.maths.models import Math
from nums_api.dates.models import Date
from flask import render_template
from nums_api.database import db, connect_db
from nums_api.config import DATABASE_URL_TEST
from nums_api.__init__ import limiter


app.config["SQLALCHEMY_DATABASE_URI"] = DATABASE_URL_TEST
app.config["TESTING"] = True
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

connect_db(app)

db.drop_all()
db.create_all()


class TestBaseEmailScript(TestCase):
    """
        Houses setup functionality.
        Should be subclassed for any year route classes utilized
    """

    def setUp(self):
        """Set up test data here"""

        Trivia.query.delete()
        Year.query.delete()
        Date.query.delete()
        Math.query.delete()
        Subscription.query.delete()
        EmailScriptLog.query.delete()

        self.e1 = EmailScriptLog(num_facts=0, num_subscribers=0)
        db.session.add(self.e1)
        db.session.commit()

        self.t1 = Trivia(
            number=1,
            fact_fragment="the number for this t1 test fact fragment",
            fact_statement="1 is the number for this t1 test fact statement.",
            was_submitted=False,
        )
        self.m1 = Math(
            number=1.5,
            fact_fragment="the number for this m1 test fact fragment",
            fact_statement="1.5 is the number for m1 this test fact statement.",
            was_submitted=False
        )

        self.d1 = Date(
            day_of_year=60,
            year=2000,
            fact_fragment="the number for this d1 test fact fragment.",
            fact_statement="60 is the number for this d1 test fact statement.",
            was_submitted=False
        )

        self.y1 = Year(
            year=2023,
            fact_fragment="the year for this y1 test fact_fragment",
            fact_statement="2023 is the year for this y1 test fact statement.",
            was_submitted=False
        )

        self.s1 = Subscription(email="123fake@fake.com")
        self.s2 = Subscription(email="456notfake@notfake.com")

        db.session.add_all([self.t1, self.m1, self.d1,
                           self.y1, self.s1, self.s2])
        db.session.commit()

        limiter.enabled = False

    def tearDown(self):
        """Clean up any fouled transaction."""
        db.session.rollback()


class TestEmailScript(TestBaseEmailScript):
    def test_setup(self):
        """Test to make sure tests are set up correctly"""
        test_setup_correct = True
        self.assertEqual(test_setup_correct, True)

    def test_rendered_email_body_contains_current_facts_from_database(self):
        """Test that the rendered html has facts from the database"""

        current_vol_of_facts = get_current_fact_volume()
        last_week_vol_of_facts = get_last_week_fact_volume()
        new_facts = get_new_facts()

        msg_body = render_template('email_template.html',
                                   current_vol_of_facts=current_vol_of_facts,
                                   last_week_vol_of_facts=last_week_vol_of_facts,
                                   new_facts=new_facts)

        self.assertIn(self.t1.fact_statement, msg_body)
        self.assertIn(self.m1.fact_statement, msg_body)
        self.assertIn(self.d1.fact_statement, msg_body)
        self.assertIn(self.y1.fact_statement, msg_body)

    def test_email_sent_to_all_subscribers_when_new_facts_added(self):
        """Test an email is sent when there is update to fact vol"""
        with mock.patch.object(Mail, "send", return_value=True) as mock_send:
            send_new_fact_email()
            mock_send.assert_called_once()
            self.assertEqual(mock_send.call_count, 1)

    def test_no_email_sent_when_not_facts_in_database(self):
        """Test an email is sent when there is update to fact vol"""
        with mock.patch.object(Mail, "send", return_value=True) as mock_send:
            Trivia.query.delete()
            Year.query.delete()
            Date.query.delete()
            Math.query.delete()
            send_new_fact_email()
            mock_send.assert_not_called()
            self.assertEqual(mock_send.call_count, 0)
