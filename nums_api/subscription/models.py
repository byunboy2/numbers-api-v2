from datetime import datetime
from nums_api.database import db
from sqlalchemy_utils import EmailType
from sqlalchemy.orm import validates
from email_validator import validate_email, EmailNotValidError


class EmailScriptLog(db.Model):
    """
    Data updated and checked every Friday to determine
    whether or not send notification email out.
    """

    __tablename__ = "email_script_logs"

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
    )

    num_facts = db.Column(
        db.Integer,
        nullable=False,
    )

    checked_at = db.Column(
        db.DateTime,
        nullable=False,
        default=datetime.utcnow()
    )

    num_subscribers = db.Column(
        db.Integer,
        nullable=False,
    )

    email_sent = db.Column(
        db.Boolean,
        default=False
    )


class Subscription(db.Model):
    """Emails of users that are subscribed"""

    __tablename__ = "subscriptions"

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True
    )

    email = db.Column(
        EmailType,
        unique=True,
        nullable=False)
    @validates('email')
    def it_validates_email(self, key, email):
        try:
            validated = validate_email(email)
            email = validated["email"]
        except EmailNotValidError:
            raise ValueError("Invalid email address.")
        return email
