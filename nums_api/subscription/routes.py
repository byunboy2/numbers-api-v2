from flask import Blueprint, jsonify, request, Flask
from nums_api.subscription.models import Subscription
from nums_api.database import db
from sqlalchemy.exc import IntegrityError
from nums_api.subscription.form_validator import SubscriptionForm

subscriptions = Blueprint("subscriptions", __name__)



@subscriptions.route("/add_subscriber", methods=['POST'])
def add_subscriber():
    """
    Add a new email to the subscription database.
    Checks if email is valid and email has yet to be added
    """

    received = request.form

    form = SubscriptionForm(csrf_enabled=False, data=received)

    if form.validate_on_submit():
        try:
            new_subscriber = Subscription(email=received["newemail"])
            db.session.add(new_subscriber)
            db.session.commit()
        except IntegrityError:
            return jsonify(error="This email is already on the subscription list"), 400

        return jsonify(message="Thank you for subscribing!"), 201

    else:

        return jsonify(error="Invalid email address!"), 400
